function saveOptions(e) {
  //update user configuration
  switch (e.target.id) {
    case "sponsor":
      browser.storage.local.set({
        sponsor: e.target.checked
      });
      break;
    case "suggest":
      browser.storage.local.set({
        suggest: e.target.checked
      });
      break;
    case "reel":
      browser.storage.local.set({
        reel: e.target.checked
      });
      break;
  }
}

function restoreOptions() {
  let sponsor = browser.storage.local.get('sponsor');
  sponsor.then((res) => {
    document.querySelector("#sponsor").checked = res.sponsor!=null?res.sponsor:true;
  });
  let suggest = browser.storage.local.get('suggest');
  suggest.then((res) => {
    document.querySelector("#suggest").checked = res.suggest!=null?res.suggest:true;
  });
  let reel = browser.storage.local.get('reel');
  reel.then((res) => {
    document.querySelector("#reel").checked = res.reel!=null?res.reel:true;
  });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
let boxes = document.querySelectorAll("input");
for (let i = 0; i < boxes.length; i++) {
    boxes[i].addEventListener("change", saveOptions);
} 