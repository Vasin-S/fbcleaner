let processSponsor = true; //I prefer to remove sponsored posts first, check configuration later.
let processSuggest = null;
let processReel = null;

let focusInEvent = new FocusEvent('focusin', {
  'view': window,
  'bubbles': true,
  'cancelable': true
});
let focusOutEvent = new FocusEvent('focusout', {
  'view': window,
  'bubbles': true,
  'cancelable': true
});

function retrieveConfiguration() {
  let sponsor = browser.storage.local.get('sponsor');
  sponsor.then((res) => {
    processSponsor = res.sponsor!=null?res.sponsor:true;
    let feed = document.querySelector("div[role='feed']");
    if (feed == null) {
      //feed was changed to main
      feed = document.querySelector("div[role='main']");
    }
    if (feed) {
      window.setTimeout(function(){ removeSponsoredPost() }, 10);
    }
  });

  let suggest = browser.storage.local.get('suggest');
  suggest.then((res) => {
    processSuggest = res.suggest!=null?res.suggest:true;
    let feed = document.querySelector("div[role='main']");
    if (feed) {
      window.setTimeout(function(){ removeSuggestedPost() }, 10);
    }
  });

  let reel = browser.storage.local.get('reel');
    reel.then((res) => {
      processReel = res.reel!=null?res.reel:true;
      let feed = document.querySelector("div[role='main']");
      if (feed) {
        window.setTimeout(function(){ removeReel() }, 10);
      }
    });
}

function renameRightRail() {
  let rightRail = document.querySelector("div[data-pagelet='RightRail']");
  if (rightRail !== null) {
    rightRail.setAttribute('data-pagelet', 'NoRail');
  }
}

function removeSponsoredPost() {
  if (processSponsor == null || !processSponsor) return;

  let feed = document.querySelector("div[role='main']");

  if (feed) {

    //latest version
    let aTagArray = feed.querySelectorAll('a[role="link"][target="_blank"]:not([href*="https:"]):not([routed=\'true\'])');
    if (aTagArray != null) {
      aTagArray.forEach(aTag => {
        aTag.setAttribute("routed", "true");
        aTag.dispatchEvent(focusInEvent);
        aTag.dispatchEvent(focusOutEvent);
      });
    }

    //previous version
    let aTagArrayBackup = feed.querySelectorAll('a[href="#"]:not([routed=\'true\'])');
    if (aTagArrayBackup != null) {
      aTagArrayBackup.forEach(aTagBackup => {
        aTagBackup.setAttribute("routed", "true");
        aTagBackup.dispatchEvent(focusInEvent);
        aTagBackup.dispatchEvent(focusOutEvent);
      });
    }

    window.setTimeout(function(){
      let targetArray = feed.querySelectorAll("a[href*='/ads/about/']");
      if (targetArray != null) {
        targetArray.forEach(target => {
            let removeTarget = target.closest("div[role='article']");
            if (removeTarget == null) {
                removeTarget = target.closest("div[aria-posinset]")
            }
            removeTarget.remove();
        });
        return;
      }
    }, 500);


    //fallback1
    let fallback1 = feed.querySelector("a[aria-label='Sponsored']");
    if (fallback1 != null) {
      fallback1.removeAttribute("aria-label");
      //fallback1.closest("div[role='article']").setAttribute("style", "border-style: dotted; border-color: red");
      fallback1.closest("div[role='article']").remove();
      return;
    }

    //fallback2
    let fallback2 = feed.querySelector("div[aria-label='Sponsored']");
    if (fallback2 != null) {
      fallback2.removeAttribute("aria-label");
      //fallback2.closest("div[role='article']").setAttribute("style", "border-style: dotted; border-color: red");
      fallback2.closest("div[role='article']").remove();
      return;
    }

  }
}

function removeSuggestedPost() {
  if (processSuggest == null || !processSuggest) return;
  let feed = document.querySelector("div[role='main']");

  if (!feed) { return; }

  let target = feed.querySelectorAll("h4:not([reviewed='true'])");
  target.forEach(function(t) {
    t.setAttribute("reviewed", "true");

    if (t.textContent !== null &&
        t.textContent.includes("·")) {
        let removeTarget = t.closest("div[aria-posinset]");
        //removeTarget.setAttribute("style", "border-style: dotted; border-color: red");
        removeTarget.remove();
    }
  });

  removeSuggestedPostWithOldAlgorithm(feed);
}

function removeSuggestedPostWithOldAlgorithm(feed) {
  let target = feed.querySelectorAll("h4:not([reviewedWithOldAlgorithm='true'])");
  target.forEach(function(t) {
    t.setAttribute("reviewedWithOldAlgorithm", "true");

    if (t.lastChild !== null &&
        t.lastChild.lastChild !== null &&
        t.lastChild.lastChild.role === 'button') {
        let removeTarget = t.closest("div[aria-posinset]");
        removeTarget.remove();
    } else if (t.parentNode.parentNode.nextSibling !== null &&
              t.parentNode.parentNode.nextSibling.firstChild !== null &&
              t.parentNode.parentNode.nextSibling.firstChild.firstChild !== null &&
              t.parentNode.parentNode.nextSibling.firstChild.firstChild.childNodes !== null) {
              let nodes = t.parentNode.parentNode.nextSibling.firstChild.firstChild.childNodes;
        for (let i = 0; i < nodes.length; i++) {
            if (nodes[i].firstElementChild == null) {
              //"Suggested for you" doesn't have any child elements
              let removeTarget = t.closest("div[aria-posinset]");
              removeTarget.remove();
              break;
            }
        }
      }
  });
}

function removeReel() {
  if (processReel == null || !processReel) return;
  let feed = document.querySelector("div[role='main']");

  if (!feed) { return; }
  let target = feed.querySelectorAll("a[href*='/reel/']:not([reviewed='true'])");
  target.forEach(function(t) {
    t.setAttribute("reviewed", "true");

    let removeTarget = t.closest("div[aria-posinset]");
    //removeTarget.setAttribute("style", "border-style: dotted; border-color: red");
    removeTarget.remove();
  });
}

function cleanFeed(time) {
  window.setTimeout(function() {
    removeSponsoredPost();
    removeSuggestedPost();
    removeReel();
  }, time);
}

//retrieve user configuration
retrieveConfiguration();

//handle on scroll event
let scrolling = true;
let stopScrolling;

function handleOnScroll() {
  scrolling = true;
  window.clearTimeout(stopScrolling);
  stopScrolling = setTimeout(function() {
    cleanFeed(1000);
  }, 100);
}

//handle on click event
function handleOnClick() {
  cleanFeed(1000);
}

//for page refresh
document.addEventListener("scroll", handleOnScroll);
document.addEventListener("click", handleOnClick);

//to block ads refresh
renameRightRail();

//for first page load
handleOnClick();  //trigger everything

//monitor scrolling
setInterval(function() {
    if (scrolling) {
        scrolling = false;
        cleanFeed(10);
    }
}, 200);

//listen for storage changes from the background script.
browser.storage.onChanged.addListener((changes, area) => {
  retrieveConfiguration()
});
