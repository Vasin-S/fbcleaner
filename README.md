# FBCleaner

Makes your Facebook cleaner.


### Features

- Remove sponsored posts from News Feed.
- Remove suggested posts from News Feed.
- Remove reels from News Feed.
- Turn these features on or off as you wish.
